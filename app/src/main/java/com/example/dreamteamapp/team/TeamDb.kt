package com.example.dreamteamapp.team

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "team")
data class TeamDb(

    @PrimaryKey var idTeam: Int = 0,
    var strTeam: String = "",
    var idLeague: Int = 0,
    var strStadiumThumb: String = "",
    var strDescriptionEN: String = "",
    var strTeamBadge: String = "",
    var strTeamLogo: String = ""
)

fun List<TeamDb>.asDomainModel(): List<Team> {
    return map {
        Team(
            idTeam = it.idTeam,
            idLeague = it.idLeague,
            strTeam = it.strTeam,
            strDescriptionEN = it.strDescriptionEN,
            strTeamLogo = it.strTeamLogo,
            strTeamBadge = it.strTeamBadge,
            strStadiumThumb = it.strStadiumThumb
        )
    }
}