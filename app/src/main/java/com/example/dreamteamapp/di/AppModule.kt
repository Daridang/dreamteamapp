package com.example.dreamteamapp.di

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.room.Room
import com.example.dreamteamapp.model.db.AppDB
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class AppModule(private val app: Application) {

    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun provideDatabase(app: Application): AppDB = Room.databaseBuilder(
        app,
        AppDB::class.java,
        "nba_db"
    ).build()

    @Provides
    @Singleton
    fun provideSharedPrefsEditor(app: Application): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(app)
    }
}