package com.example.dreamteamapp.di

import androidx.lifecycle.ViewModelProvider
import com.example.dreamteamapp.utils.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelFactoryModule {

    @Binds
    abstract fun bindFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory
}
