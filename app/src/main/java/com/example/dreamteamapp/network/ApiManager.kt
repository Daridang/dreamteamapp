package com.example.dreamteamapp.network

import com.example.dreamteamapp.event.Events
import com.example.dreamteamapp.player.Players
import com.example.dreamteamapp.team.Teams
import io.reactivex.Observable
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiManager {

    @GET("lookup_all_teams.php?")
    fun getAllTeams(@Query("id") leagueId: Int): Observable<Teams>

    @GET("searchplayers.php?")
    fun getAllPlayersInTeam(@Query("t") teamName: String): Deferred<Players>

    @GET("eventslast.php?")
    fun getEventsForTeamById(@Query("id") teamID: Int): Deferred<Events>
}