package com.example.dreamteamapp.team

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.dreamteamapp.MainActivity
import com.example.dreamteamapp.R
import com.example.dreamteamapp.utils.ViewModelFactory
import com.example.dreamteamapp.databinding.TeamDetailsFragmentBinding
import com.example.dreamteamapp.event.EventsFragment
import com.example.dreamteamapp.player.PlayersFragment
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class TeamDetailsFragment : DaggerFragment() {

    companion object {
        fun newInstance() = TeamDetailsFragment()
    }

    private lateinit var viewModel: TeamDetailsViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var binding: TeamDetailsFragmentBinding
    private lateinit var viewPager: ViewPager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil
            .inflate(inflater, R.layout.team_details_fragment, container, false)

        val tabLayout = binding.tabLayout
        viewPager = binding.viewPagerId
        tabLayout.setupWithViewPager(viewPager)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProvider(this, viewModelFactory).get(TeamDetailsViewModel::class.java)
        val args = arguments?.let { TeamListFragmentArgs.fromBundle(it) }


        if (args != null) {
            val team = args.team
            viewModel.setTeam(team)

            val pagerAdapter = PagerAdapter(
                team,
                childFragmentManager,
                BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
            )

            viewPager.adapter = pagerAdapter

        }

        viewModel.getTeam().observe(this, Observer {
            Glide.with(binding.teamImage.context)
                .load(viewModel.getTeam().value?.strStadiumThumb)
                .apply(
                    RequestOptions()
                        .placeholder(R.drawable.loading_animation)
                        .error(R.drawable.ic_broken_image)
                ).into(binding.teamImage)
            binding.item = viewModel.getTeam().value
        })

        binding.backBtn.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        (activity as MainActivity).toolbar.visibility = View.GONE
    }

    override fun onStop() {
        super.onStop()
        (activity as MainActivity).toolbar.visibility = View.VISIBLE
    }
}

class PagerAdapter(team: Team, fm: FragmentManager, behavior: Int) :
    FragmentStatePagerAdapter(fm, behavior) {

    companion object {
        val titles = listOf("News", "Players")
    }

    private var pos: Int
    private var args: Bundle
    private var events: EventsFragment
    private var players: PlayersFragment

    init {
        pos = titles.size
        args = Bundle()
        args.putParcelable("team", team)
        events = EventsFragment.newInstance()
        events.arguments = args
        players = PlayersFragment.newInstance()
        players.arguments = args
    }

    override fun getItem(position: Int): Fragment {

        return when (position) {
            0 -> events
            1 -> players
            else -> throw Throwable()
        }
    }

    override fun getCount(): Int {
        return pos
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return titles[position]
    }
}