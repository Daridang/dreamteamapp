package com.example.dreamteamapp.player

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.dreamteamapp.R
import com.example.dreamteamapp.utils.ViewModelFactory
import com.example.dreamteamapp.databinding.ListFragmentBinding
import com.example.dreamteamapp.team.Team
import dagger.android.support.DaggerFragment
import javax.inject.Inject


class PlayersFragment : DaggerFragment() {

    companion object {
        fun newInstance() = PlayersFragment()
    }

    private var teamId: Int = 0
    private lateinit var team: Team

    private lateinit var playerViewModel: PlayerViewModel
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var binding: ListFragmentBinding

    private lateinit var adapter: PlayersAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.list_fragment, container, false
        )

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        playerViewModel = ViewModelProvider(this, viewModelFactory).get(PlayerViewModel::class.java)

        initRecyclerView()

//        playerViewModel.players.observe(this, Observer {
//
//        })

        if (arguments != null) {
            team = arguments?.getParcelable("team")!!

            playerViewModel.getPlayers(team.strTeam, teamId)
                .observe(this, Observer {
                    adapter.players = it ?: listOf()
                })

        }

    }

    private fun initRecyclerView() {
        adapter = PlayersAdapter()
        binding.recyclerViewId.layoutManager = LinearLayoutManager(activity)
        binding.recyclerViewId.adapter = adapter

        val dividerItemDecoration =
            DividerItemDecoration(
                binding.recyclerViewId.context,
                DividerItemDecoration.VERTICAL
            )
        dividerItemDecoration.setDrawable(resources.getDrawable(R.drawable.divider, null))
        binding.recyclerViewId.addItemDecoration(dividerItemDecoration)
    }
}
