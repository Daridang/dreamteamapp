package com.example.dreamteamapp.model.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.dreamteamapp.event.EventDb
import com.example.dreamteamapp.team.TeamDb
import com.example.dreamteamapp.model.dao.EventDAO
import com.example.dreamteamapp.model.dao.PlayerDAO
import com.example.dreamteamapp.model.dao.TeamDAO
import com.example.dreamteamapp.player.PlayerDb

@Database(
    entities = [TeamDb::class, PlayerDb::class, EventDb::class],
    version = 1,
    exportSchema = false
)
abstract class AppDB : RoomDatabase() {

    abstract fun teamDAO(): TeamDAO
    abstract fun playerDAO(): PlayerDAO
    abstract fun eventDAO(): EventDAO

}