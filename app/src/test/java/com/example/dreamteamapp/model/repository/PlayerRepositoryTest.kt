package com.example.dreamteamapp.model.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.dreamteamapp.model.dao.PlayerDAO
import com.example.dreamteamapp.model.db.AppDB
import com.example.dreamteamapp.network.ApiClient
import com.example.dreamteamapp.network.ApiManager
import com.example.dreamteamapp.player.PlayerDb
import kotlinx.coroutines.*
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations


@RunWith(JUnit4::class)
class PlayerRepositoryTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var apiClient: ApiClient

    @Mock
    lateinit var apiManager: ApiManager

    @Mock
    lateinit var db: AppDB

    @Mock
    lateinit var playerDAO: PlayerDAO

    lateinit var playerRepo: PlayerRepository

    @Before
    fun init() {
        MockitoAnnotations.initMocks(this)
        playerRepo = PlayerRepository(db, apiClient)
    }

    @Test
    fun `should return list with one player when getPlayerById() called`() {
        runBlocking(Dispatchers.IO) {
            `when`(playerDAO.getPlayerById(1)).thenReturn(listOf(PlayerDb(idPlayer = 1)))
            val p = playerDAO.getPlayerById(1)
            assertEquals(1, p[0].idPlayer)
        }
    }

    @Test
    fun `return not null player when getPlayersByTeamId called`() {
        `when`(playerDAO.getPlayersByTeamId(56)).thenReturn(MutableLiveData<List<PlayerDb>>(listOf(
            PlayerDb(idPlayer = 45)
        )))
        val p = playerDAO.getPlayersByTeamId(56)
        assertNotNull(p.value?.get(0))
    }

    @Test
    fun `return null when getPlayersByTeamId called`() {
        `when`(playerDAO.getPlayersByTeamId(56)).thenReturn(null)
        val p = playerDAO.getPlayersByTeamId(56)
        assertNull(p)
    }
}