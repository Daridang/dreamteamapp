package com.example.dreamteamapp.di

import androidx.lifecycle.ViewModel
import com.example.dreamteamapp.team.*
import com.example.dreamteamapp.utils.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class TeamModule {

    @ContributesAndroidInjector
    abstract fun contributeTeamListFragment(): TeamListFragment

    @ContributesAndroidInjector
    abstract fun contributeTeamDetailsFragment(): TeamDetailsFragment

    @IntoMap
    @ViewModelKey(TeamViewModel::class)
    @Binds
    abstract fun bindTeamViewModel(vm: TeamViewModel): ViewModel

    @IntoMap
    @ViewModelKey(TeamDetailsViewModel::class)
    @Binds
    abstract fun bindTeamDetailsViewModel(vm: TeamDetailsViewModel): ViewModel
}