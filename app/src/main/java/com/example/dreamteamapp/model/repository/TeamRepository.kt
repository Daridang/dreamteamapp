package com.example.dreamteamapp.model.repository

import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.dreamteamapp.utils.LEAGUE_ID
import com.example.dreamteamapp.utils.TEAM_UPDATE
import com.example.dreamteamapp.utils.UpdateTime
import com.example.dreamteamapp.model.db.AppDB
import com.example.dreamteamapp.network.ApiClient
import com.example.dreamteamapp.team.Team
import com.example.dreamteamapp.team.asDataBaseModel
import com.example.dreamteamapp.team.asDomainModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class TeamRepository @Inject constructor(
    private val database: AppDB,
    private val apiClient: ApiClient
) : BaseRepository {

    @Inject
    lateinit var prefs: SharedPreferences

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    fun getList(): LiveData<List<Team>> {
        val list: MutableLiveData<List<Team>> = MutableLiveData()

        // Get list of teams from database
        compositeDisposable.add(
            database.teamDAO().getTeamList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ onSuccess ->
                    if (onSuccess.isNullOrEmpty()) {
                        fetchFromAPI(list)
                    } else {
                        list.postValue(onSuccess.asDomainModel())
                    }
                    prefs.edit().putLong(TEAM_UPDATE, System.currentTimeMillis()).apply()
                },
                    { onError ->
                        Log.d("TAGG", "onError ${onError.fillInStackTrace()}")
                    }
                )
        )

        checkForTimeToRefresh()
        return list
    }

    private fun checkForTimeToRefresh() {
        // If last update time for team list not 0, and is more than updating frequency
        if (prefs.getLong(TEAM_UPDATE, 0) != 0L && System.currentTimeMillis()
            - prefs.getLong(TEAM_UPDATE, 0) > UpdateTime.TEAM.int
        ) {

            refreshTeamList()
            prefs.edit().putLong(TEAM_UPDATE, System.currentTimeMillis()).apply()
        }
    }

    private fun fetchFromAPI(list: MutableLiveData<List<Team>>) {
        val teams = apiClient.get().getAllTeams(LEAGUE_ID)

        // Get data from API
        compositeDisposable.add(teams
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    list.postValue(it.asDomainModel())
                    database.teamDAO().insert(it.asDataBaseModel())
                },
                {
                    Log.d("TAGG", " error api: ${it.fillInStackTrace()}")
                }
            )
        )
    }

    private fun refreshTeamList() {
        val teams = apiClient.get().getAllTeams(LEAGUE_ID)
        // Save to database
        compositeDisposable.add(teams
            .subscribeOn(Schedulers.io())
            .subscribe(
                { database.teamDAO().insert(it.asDataBaseModel()) },
                { Log.d("TAGG", " error refresh: ${it.fillInStackTrace()}") }
            )
        )
    }
}