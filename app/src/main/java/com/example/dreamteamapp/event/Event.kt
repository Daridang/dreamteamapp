package com.example.dreamteamapp.event


data class Event(

    var idEvent: Int = 0,
    var idLeague: Int = 0,
    var strHomeTeam: String = "",
    var strAwayTeam: String = "",
    var idHomeTeam: Int = 0,
    var idAwayTeam: Int = 0,
    var dateEvent: String = ""
)

fun List<Event>.asDatabaseModel(): List<EventDb> {
    return map {
        EventDb(
            idEvent = it.idEvent,
            idLeague = it.idLeague,
            strHomeTeam = it.strHomeTeam,
            strAwayTeam = it.strAwayTeam,
            idHomeTeam = it.idHomeTeam,
            idAwayTeam = it.idAwayTeam,
            dateEvent = it.dateEvent
        )
    }
}