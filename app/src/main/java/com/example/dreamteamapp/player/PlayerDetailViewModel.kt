package com.example.dreamteamapp.player

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.dreamteamapp.model.repository.PlayerRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class PlayerDetailViewModel @Inject constructor(private val repo: PlayerRepository) : ViewModel() {

    fun getPlayer(playerId: Int): LiveData<Player> {
        val player = MutableLiveData<Player>()
        viewModelScope.launch {
            val list = repo.getPlayer(playerId)
            player.value = list.value?.find { playerId == it.idPlayer }
        }
        return player
    }
}