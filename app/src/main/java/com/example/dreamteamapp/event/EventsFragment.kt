package com.example.dreamteamapp.event

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.dreamteamapp.R
import com.example.dreamteamapp.utils.ViewModelFactory
import com.example.dreamteamapp.databinding.ListFragmentBinding
import com.example.dreamteamapp.team.Team
import dagger.android.support.DaggerFragment
import javax.inject.Inject


class EventsFragment : DaggerFragment() {

    companion object {
        fun newInstance() = EventsFragment()
    }

    private lateinit var viewModel: EventViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var binding: ListFragmentBinding

    private lateinit var adapter: EventsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.list_fragment, container, false)

        adapter = EventsAdapter()

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProvider(this, viewModelFactory).get(EventViewModel::class.java)

        if (arguments != null) {
            val team = arguments?.getParcelable<Team>("team")

            viewModel.getEventsById(team!!.idTeam).observe(this, Observer {
                adapter.events = it
            })

        }

        iniRecyclerView()
    }

    private fun iniRecyclerView() {
        binding.recyclerViewId.layoutManager = LinearLayoutManager(activity)
        binding.recyclerViewId.adapter = adapter

        val dividerItemDecoration =
            DividerItemDecoration(binding.recyclerViewId.context, DividerItemDecoration.VERTICAL)
        dividerItemDecoration.setDrawable(resources.getDrawable(R.drawable.divider, null))
        binding.recyclerViewId.addItemDecoration(dividerItemDecoration)
    }

}
