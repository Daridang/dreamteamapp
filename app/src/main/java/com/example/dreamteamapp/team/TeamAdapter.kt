package com.example.dreamteamapp.team

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.dreamteamapp.R
import com.example.dreamteamapp.databinding.TeamListItemBinding


class TeamAdapter :
    RecyclerView.Adapter<TeamAdapter.TeamViewHolder>() {

    var teams: List<Team> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding =
            DataBindingUtil.inflate<TeamListItemBinding>(
                inflater,
                R.layout.team_list_item, parent, false
            )
        return TeamViewHolder(binding)
    }

    override fun getItemCount() = teams.size

    override fun onBindViewHolder(holder: TeamViewHolder, position: Int) {
        holder.bind(teams[position])
    }

    inner class TeamViewHolder(val binding: TeamListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(team: Team) {

            Glide.with(binding.teamIconId.context)
                .load(team.strTeamBadge)
                .apply(
                    RequestOptions()
                        .placeholder(R.drawable.loading_animation)
                        .error(R.drawable.ic_broken_image)
                ).into(binding.teamIconId)

            binding.item = team


            binding.root.setOnClickListener {
                it.findNavController()
                    .navigate(R.id.teamDetailsFragment, TeamListFragmentArgs(team).toBundle())
            }
        }
    }
}