package com.example.dreamteamapp.player

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.dreamteamapp.R
import com.example.dreamteamapp.databinding.PlayersItemBinding


class PlayersAdapter:
    RecyclerView.Adapter<PlayersAdapter.PlayersViewHolder>() {

    var players = listOf<Player>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayersViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding =
            DataBindingUtil.inflate<PlayersItemBinding>(
                inflater,
                R.layout.players_item, parent, false
            )
        return PlayersViewHolder(binding)
    }

    override fun getItemCount() = players.size

    override fun onBindViewHolder(holder: PlayersViewHolder, position: Int) {
        holder.bind(players[position])
    }

    inner class PlayersViewHolder(val binding: PlayersItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(player: Player) {
            binding.item = player
            Glide.with(binding.playersAvatar.context)
                .load(player.strThumb)
                .apply(
                    RequestOptions()
                        .circleCrop()
                        .placeholder(R.drawable.loading_animation)
                        .error(R.drawable.ic_broken_image)
                ).into(binding.playersAvatar)
            binding.root.setOnClickListener {
                it.findNavController()
                    .navigate(
                        R.id.playersDetailsFragment,
                        PlayersFragmentArgs(player.idPlayer!!).toBundle()
                    )
            }
        }
    }
}