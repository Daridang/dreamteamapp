package com.example.dreamteamapp.player

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.dreamteamapp.model.repository.PlayerRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by
+-+-+-+-+-+-+-+-+
|D|a|r|i|d|a|n|g|
+-+-+-+-+-+-+-+-+
on 2020-01-24.
 */
class PlayerViewModel @Inject constructor(private val repo: PlayerRepository) : ViewModel() {

//    private var _players: LiveData<List<Player>> = MutableLiveData<List<Player>>()
//    val players = _players
//
//
//    init {
//        viewModelScope.launch {
//            _players = repo.getPlayersInTeam("", 0)
//        }
//    }

    fun getPlayers(teamName: String, teamId: Int): LiveData<List<Player>> {
        val list = MutableLiveData<List<Player>>()
        viewModelScope.launch {
            list.value = repo.getPlayersInTeam(teamName, teamId).value
        }
        return list
    }
}