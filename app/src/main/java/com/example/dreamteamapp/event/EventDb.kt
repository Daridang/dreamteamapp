package com.example.dreamteamapp.event

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "event")
data class EventDb(
    @PrimaryKey
    var idEvent: Int = 0,
    var idLeague: Int = 0,
    var strHomeTeam: String = "",
    var strAwayTeam: String = "",
    var idHomeTeam: Int = 0,
    var idAwayTeam: Int = 0,
    var dateEvent: String = ""
)

fun List<EventDb>.asDomainModel(): List<Event> {
    return map {
        Event(
            idEvent = it.idEvent,
            idLeague = it.idLeague,
            strHomeTeam = it.strHomeTeam,
            strAwayTeam = it.strAwayTeam,
            idHomeTeam = it.idHomeTeam,
            idAwayTeam = it.idAwayTeam,
            dateEvent = it.dateEvent
        )
    }
}