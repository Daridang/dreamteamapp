package com.example.dreamteamapp.team

import androidx.lifecycle.*
import javax.inject.Inject

open class TeamDetailsViewModel @Inject constructor() : ViewModel() {

    private var team = MutableLiveData<Team>()

    fun setTeam(team: Team) {
        this.team.value = team
    }

    fun getTeam(): LiveData<Team> {
        return team
    }
}
