package com.example.dreamteamapp.model.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.dreamteamapp.team.TeamDb
import io.reactivex.Single

@Dao
interface TeamDAO {

    @Query("SELECT * FROM team ORDER BY strTeam DESC")
    fun getTeamList(): Single<List<TeamDb>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(teamDb: List<TeamDb>)

    @Query("DELETE FROM team")
    suspend fun deleteAll()

    @Query("SELECT * FROM team WHERE idTeam = :id")
    fun getTeamById(id: Int): TeamDb

}