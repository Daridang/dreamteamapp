package com.example.dreamteamapp.team

import com.google.gson.annotations.SerializedName


data class Teams(@SerializedName("teams") val teams: List<TeamAPI>)

data class TeamAPI(

    val idTeam: Int = 0,
    val idSoccerXML: String = "",
    val idAPIfootball: Int = 0,
    val intLoved: String = "",
    val strTeam: String = "",
    val strTeamShort: String = "",
    val strAlternate: String = "",
    val intFormedYear: Int = 0,
    val strSport: String = "",
    val strLeague: String = "",
    val idLeague: Int = 0,
    val strDivision: String = "",
    val strManager: String = "",
    val strStadium: String = "",
    val strKeywords: String = "",
    val strRSS: String = "",
    val strStadiumThumb: String = "",
    val strStadiumDescription: String = "",
    val strStadiumLocation: String = "",
    val intStadiumCapacity: Int = 0,
    val strWebsite: String = "",
    val strFacebook: String = "",
    val strTwitter: String = "",
    val strInstagram: String = "",
    val strDescriptionEN: String = "",
    val strDescriptionDE: String = "",
    val strDescriptionFR: String = "",
    val strDescriptionCN: String = "",
    val strDescriptionIT: String = "",
    val strDescriptionJP: String = "",
    val strDescriptionRU: String = "",
    val strDescriptionES: String = "",
    val strDescriptionPT: String = "",
    val strDescriptionSE: String = "",
    val strDescriptionNL: String = "",
    val strDescriptionHU: String = "",
    val strDescriptionNO: String = "",
    val strDescriptionIL: String = "",
    val strDescriptionPL: String = "",
    val strGender: String = "",
    val strCountry: String = "",
    val strTeamBadge: String = "",
    val strTeamJersey: String = "",
    val strTeamLogo: String = "",
    val strTeamFanart1: String = "",
    val strTeamFanart2: String = "",
    val strTeamFanart3: String = "",
    val strTeamFanart4: String = "",
    val strTeamBanner: String = "",
    val strYoutube: String = "",
    val strLocked: String = ""
)

fun Teams.asDomainModel(): List<Team> {
    return teams.map {
        Team(
            idTeam = it.idTeam,
            strTeam = it.strTeam,
            idLeague = it.idLeague,
            strStadiumThumb = it.strStadiumThumb,
            strDescriptionEN = it.strDescriptionEN,
            strTeamBadge = it.strTeamBadge,
            strTeamLogo = it.strTeamLogo
        )
    }
}

fun Teams.asDataBaseModel(): List<TeamDb> {
    return teams.map {
        TeamDb(
            idTeam = it.idTeam,
            strTeam = it.strTeam,
            idLeague = it.idLeague,
            strStadiumThumb = it.strStadiumThumb,
            strDescriptionEN = it.strDescriptionEN,
            strTeamBadge = it.strTeamBadge,
            strTeamLogo = it.strTeamLogo
        )
    }
}