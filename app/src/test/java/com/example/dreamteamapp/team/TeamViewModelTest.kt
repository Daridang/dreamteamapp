package com.example.dreamteamapp.team

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.dreamteamapp.model.repository.TeamRepository
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.Mockito.`when` as wen

@RunWith(AndroidJUnit4::class)
class TeamViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var repo: TeamRepository

    lateinit var viewModel: TeamViewModel

    @Mock
    lateinit var observer: Observer<List<Team>>

    @Before
    fun init() {
        MockitoAnnotations.initMocks(this)
        viewModel = TeamViewModel(repo)

    }

    @Test
    fun `return not null when called getTeamList() value`() {
        wen(repo.getList()).thenReturn(MutableLiveData(emptyList()))

        viewModel.getTeamList().observeForever(observer)
        viewModel.getTeamList().value

        assertNotNull(viewModel.getTeamList().value)
    }

    @Test
    fun `should return expected team name`() {
        wen(repo.getList()).thenReturn(
            MutableLiveData(
                listOf(
                    Team(strTeam = "name")
                )
            )
        )
        viewModel.getTeamList().observeForever(observer)

        val name = viewModel.getTeamList().value?.first()?.strTeam
        assertEquals("name", name)
    }

    @Test
    fun `should return null when repo getList() called`() {
        wen(repo.getList()).thenReturn(null)
        assertEquals(null, viewModel.getTeamList())
    }
}