package com.example.dreamteamapp.model.repository

import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.dreamteamapp.utils.EVENT_UPDATE
import com.example.dreamteamapp.utils.UpdateTime
import com.example.dreamteamapp.event.Event
import com.example.dreamteamapp.event.asDataBaseModel
import com.example.dreamteamapp.event.asDatabaseModel
import com.example.dreamteamapp.event.asDomainModel
import com.example.dreamteamapp.model.db.AppDB
import com.example.dreamteamapp.network.ApiClient
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class EventRepository @Inject constructor(
    private val database: AppDB,
    private val service: ApiClient
) : BaseRepository {

    @Inject
    lateinit var prefs: SharedPreferences

    suspend fun getEvents(id: Int): LiveData<List<Event>> {
        var list = database.eventDAO().getEventListById(id).value?.asDomainModel()
        if (list.isNullOrEmpty()) {
            list = service.get().getEventsForTeamById(id).await().asDomainModel()
            database.eventDAO().insertAll(list.asDatabaseModel())
            prefs.edit().putLong(EVENT_UPDATE, System.currentTimeMillis()).apply()
        }
        if (prefs.getLong(EVENT_UPDATE, 0) != 0L && System.currentTimeMillis()
            - prefs.getLong(EVENT_UPDATE, 0) > UpdateTime.EVENT.int
        ) {
            refreshEventList(id)
            prefs.edit().putLong(EVENT_UPDATE, System.currentTimeMillis()).apply()
        }
        return MutableLiveData<List<Event>>(list)
    }

    private suspend fun refreshEventList(id: Int) {
        withContext(Dispatchers.IO) {
            val events = service.get().getEventsForTeamById(id).await()
            database.eventDAO().insertAll(events.asDataBaseModel())
        }
    }
}