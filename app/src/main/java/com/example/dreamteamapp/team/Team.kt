package com.example.dreamteamapp.team

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Team(
    var idTeam: Int = 0,
    var strTeam: String = "",
    var idLeague: Int = 0,
    var strStadiumThumb: String = "",
    var strDescriptionEN: String = "",
    var strTeamBadge: String = "",
    var strTeamLogo: String = ""
) : Parcelable

fun List<Team>.asDatabaseModel(): List<TeamDb> {
    return map {
        TeamDb(
            idTeam = it.idTeam,
            idLeague = it.idLeague,
            strTeam = it.strTeam,
            strDescriptionEN = it.strDescriptionEN,
            strTeamLogo = it.strTeamLogo,
            strTeamBadge = it.strTeamBadge,
            strStadiumThumb = it.strStadiumThumb
        )
    }
}