package com.example.dreamteamapp.event

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.dreamteamapp.model.repository.EventRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class EventViewModel @Inject constructor(private val repo: EventRepository) : ViewModel() {

    fun getEventsById(teamId: Int): LiveData<List<Event>> {
        val list = MutableLiveData<List<Event>>()
        viewModelScope.launch {
            list.value = repo.getEvents(teamId).value
        }
        return list
    }
}