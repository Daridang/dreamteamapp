package com.example.dreamteamapp.model.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.dreamteamapp.event.EventDb

@Dao
interface EventDAO {
    @Query("SELECT * FROM event ORDER BY dateEvent ASC")
    fun getEventList(): LiveData<List<EventDb>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(eventDb: List<EventDb>)

    @Query("DELETE FROM event")
    suspend fun deleteAll()

    @Query("SELECT * FROM event WHERE  idHomeTeam = :id OR idAwayTeam = :id")
    fun getEventListById(id: Int): LiveData<List<EventDb>>
}