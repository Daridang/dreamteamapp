package com.example.dreamteamapp.utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton

/**
 * Created by
+-+-+-+-+-+-+-+-+
|D|a|r|i|d|a|n|g|
+-+-+-+-+-+-+-+-+
on 2020-01-29.
 */

@Singleton
class ViewModelFactory
@Inject constructor(private val viewModelMap: @JvmSuppressWildcards Map<Class<out ViewModel>, Provider<ViewModel>>) :
    ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>) =
        viewModelMap.getValue(modelClass).get() as T
}
