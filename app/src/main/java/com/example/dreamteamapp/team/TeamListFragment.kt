package com.example.dreamteamapp.team

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.dreamteamapp.R
import com.example.dreamteamapp.utils.ViewModelFactory
import com.example.dreamteamapp.databinding.ListFragmentBinding
import dagger.android.support.DaggerFragment
import javax.inject.Inject


class TeamListFragment : DaggerFragment() {

    companion object {
        fun newInstance() =
            TeamListFragment()
    }

    private lateinit var teamViewModel: TeamViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var binding: ListFragmentBinding

    private lateinit var adapter: TeamAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater, R.layout.list_fragment, container, false
        )
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initRecyclerView()

        teamViewModel = ViewModelProvider(this, viewModelFactory).get(TeamViewModel::class.java)
        teamViewModel.getTeamList().observe(this, Observer<List<Team>> { teams ->
            teams?.apply {
                adapter.teams = teams
            }
        })
    }

    private fun initRecyclerView() {
        adapter = TeamAdapter()

        binding.recyclerViewId.layoutManager = LinearLayoutManager(activity!!)
        binding.recyclerViewId.adapter = adapter
    }
}
