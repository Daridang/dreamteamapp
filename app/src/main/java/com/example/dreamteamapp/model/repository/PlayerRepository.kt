package com.example.dreamteamapp.model.repository

import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.dreamteamapp.utils.PLAYER_UPDATE
import com.example.dreamteamapp.utils.UpdateTime
import com.example.dreamteamapp.model.db.AppDB
import com.example.dreamteamapp.network.ApiClient
import com.example.dreamteamapp.player.Player
import com.example.dreamteamapp.player.asDataBaseModel
import com.example.dreamteamapp.player.asDatabaseModel
import com.example.dreamteamapp.player.asDomainModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class PlayerRepository @Inject constructor(
    private val database: AppDB,
    private val apiClient: ApiClient
) : BaseRepository {

    @Inject
    lateinit var prefs: SharedPreferences

    suspend fun getPlayersInTeam(team: String, id: Int): LiveData<List<Player>> {

        var list = database.playerDAO().getPlayersByTeamId(id).value?.asDomainModel()
        if (list.isNullOrEmpty()) {
            val name = team.replace(" ", "_")
            list = apiClient.get().getAllPlayersInTeam(name).await().asDomainModel()
            database.playerDAO().insertAll(list.asDatabaseModel())
            prefs.edit().putLong(PLAYER_UPDATE, System.currentTimeMillis()).apply()
        }
        if (prefs.getLong(PLAYER_UPDATE, 0) != 0L && System.currentTimeMillis()
            - prefs.getLong(PLAYER_UPDATE, 0) > UpdateTime.PLAYER.int
        ) {
            refreshPlayersList(team)
            prefs.edit().putLong(PLAYER_UPDATE, System.currentTimeMillis()).apply()
        }
        return MutableLiveData<List<Player>>(list)

    }

    suspend fun getPlayer(playerId: Int): LiveData<List<Player>> {
        val list = database.playerDAO().getPlayerById(playerId).asDomainModel()
        return MutableLiveData<List<Player>>(list)
    }

    private suspend fun refreshPlayersList(teamName: String) {
        withContext(Dispatchers.IO) {
            val teamN = teamName.replace(" ", "_")
            val players = apiClient.get().getAllPlayersInTeam(teamN).await()
            database.playerDAO().insertAll(players.asDataBaseModel())
        }
    }
}