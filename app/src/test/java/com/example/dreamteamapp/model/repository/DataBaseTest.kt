package com.example.dreamteamapp.model.repository


import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.dreamteamapp.model.db.AppDB
import com.example.dreamteamapp.team.TeamDb
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.MockitoAnnotations
import java.io.IOException


@RunWith(AndroidJUnit4::class)
class DataBaseTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    lateinit var db: AppDB

    @Before
    fun init() {
        db = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDB::class.java
        ).build()
        MockitoAnnotations.initMocks(this)
    }

    @After
    @Throws(IOException::class)
    fun close() {
        db.close()
    }

    @Test
    fun `should return id equal to inserted Team's id`() {
        runBlocking(Dispatchers.IO) {
            val team = TeamDb(idTeam = 234)

            db.teamDAO().insert(listOf(team))

            val teamFromDB = db.teamDAO().getTeamById(234)

            assertEquals(team.idTeam, teamFromDB.idTeam)
        }
    }

    @Test
    fun `should return Null after Team is deleted`() {
        runBlocking(Dispatchers.IO) {
            val team = TeamDb(idTeam = 234)

            db.teamDAO().insert(listOf(team))
            var teamFromDB = db.teamDAO().getTeamById(234)
            assertEquals(team.idTeam, teamFromDB.idTeam)
            db.teamDAO().deleteAll()

            teamFromDB = db.teamDAO().getTeamById(234)
            assertNull(teamFromDB)
        }
    }
}