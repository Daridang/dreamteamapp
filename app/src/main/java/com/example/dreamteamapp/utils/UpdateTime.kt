package com.example.dreamteamapp.utils

enum class UpdateTime(val int: Int) {
    TEAM(3600000), EVENT(900000), PLAYER(1800000)
}