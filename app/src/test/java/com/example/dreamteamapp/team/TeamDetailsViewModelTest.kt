package com.example.dreamteamapp.team

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations

@RunWith(AndroidJUnit4::class)
class TeamDetailsViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var team: Team

    lateinit var viewModel: TeamDetailsViewModel

    @Before
    fun init() {
        MockitoAnnotations.initMocks(this)
        viewModel = TeamDetailsViewModel()

    }

    @Test
    fun `should return not null when team set`() {
        viewModel.setTeam(team)
        assertNotNull(viewModel.getTeam().value)
    }

    @Test
    fun `should return null if team is not set`() {
        assertNull(viewModel.getTeam().value)
    }

    @Test
    fun `return true when team name changed`() {
        team.strTeam = "First"
        viewModel.setTeam(team)
        val first = viewModel.getTeam().value?.strTeam
        viewModel.setTeam(Team(strTeam = "Second"))
        assertNotEquals(first, viewModel.getTeam().value?.strTeam)
    }
}