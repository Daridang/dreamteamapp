package com.example.dreamteamapp.player

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.dreamteamapp.MainActivity
import com.example.dreamteamapp.R
import com.example.dreamteamapp.utils.ViewModelFactory
import com.example.dreamteamapp.databinding.PlayersDetailsFragmentBinding
import dagger.android.support.DaggerFragment
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class PlayersDetailsFragment : DaggerFragment() {

    companion object {
        fun newInstance() =
            PlayersDetailsFragment()
    }

    private lateinit var viewModel: PlayerDetailViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var binding: PlayersDetailsFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.players_details_fragment, container, false
        )
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProvider(this, viewModelFactory).get(PlayerDetailViewModel::class.java)

        val args = arguments?.let { PlayersFragmentArgs.fromBundle(it) }

        if (args != null) {
            val playerId = args.playerId

            viewModel.getPlayer(playerId).observe(this, Observer {
                val player = it
                if (player != null) {
                    binding.item = player
                    player.dateBorn = getAge(player.dateBorn!!).toString()
                    loadPlayersPicture(player)
                }
            })
        }

        binding.backBtn.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
    }

    private fun getAge(dobString: String): Int {
        var date: Date? = null
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        try {
            date = sdf.parse(dobString)
        } catch (e: Throwable) {
            e.printStackTrace()
        }
        if (date == null) return 0
        val dob = Calendar.getInstance()
        val today = Calendar.getInstance()
        dob.time = date
        val year = dob[Calendar.YEAR]
        val month = dob[Calendar.MONTH]
        val day = dob[Calendar.DAY_OF_MONTH]
        dob[year, month + 1] = day
        var age = today[Calendar.YEAR] - dob[Calendar.YEAR]
        if (today[Calendar.DAY_OF_YEAR] < dob[Calendar.DAY_OF_YEAR]) {
            age--
        }
        return age
    }

    override fun onResume() {
        super.onResume()
        (activity as MainActivity).toolbar.visibility = View.GONE
    }

    override fun onStop() {
        super.onStop()
        (activity as MainActivity).toolbar.visibility = View.VISIBLE
    }

    private fun loadPlayersPicture(player: Player) {

        Glide.with(binding.playersImage.context)
            .load(player.strThumb)
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.loading_animation)
                    .error(R.drawable.ic_broken_image)
            ).into(binding.playersImage)
    }

}
