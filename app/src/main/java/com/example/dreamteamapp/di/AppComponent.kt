package com.example.dreamteamapp.di

import android.app.Application
import com.example.dreamteamapp.NbaApp
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBindingModule::class,
        AppModule::class,
        TeamModule::class,
        PlayerModule::class,
        EventModule::class,
        ViewModelFactoryModule::class
    ]
)
interface AppComponent : AndroidInjector<NbaApp> {

    fun inject(app: Application)

}
