package com.example.dreamteamapp.model.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.dreamteamapp.player.PlayerDb

@Dao
interface PlayerDAO {

    @Query("SELECT * FROM player WHERE idTeam = :teamId ORDER BY strPlayer ASC")
    fun getPlayersByTeamId(teamId: Int): LiveData<List<PlayerDb>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(playerDb: List<PlayerDb>)

    @Query("DELETE FROM player")
    suspend fun deleteAll()

    @Query("SELECT * FROM player WHERE idPlayer = :id")
    suspend fun getPlayerById(id: Int): List<PlayerDb>
}