package com.example.dreamteamapp.event

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.dreamteamapp.R
import com.example.dreamteamapp.databinding.NewsItemBinding


class EventsAdapter :
    RecyclerView.Adapter<EventsAdapter.NewsViewHolder>() {

    var events = emptyList<Event>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding =
            DataBindingUtil.inflate<NewsItemBinding>(
                inflater,
                R.layout.news_item, parent, false
            )
        return NewsViewHolder(binding)
    }

    override fun getItemCount() = events.size

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.bind(events[position])
    }

    inner class NewsViewHolder(val binding: NewsItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(n: Event) {
            binding.item = n
        }
    }
}