package com.example.dreamteamapp

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import com.example.dreamteamapp.team.TeamAdapter
import org.hamcrest.core.AllOf.allOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@LargeTest
class TeamDetailsFragmentTest {

    @get:Rule
    val activityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun test() {
        onView(allOf(withId(R.id.recycler_view_id), isDisplayed())).perform(
            RecyclerViewActions.actionOnItemAtPosition<TeamAdapter.TeamViewHolder>(0, click())
        )

        onView(withId(R.id.back_btn)).perform(click())

        onView(allOf(withId(R.id.recycler_view_id), isDisplayed())).perform(
            RecyclerViewActions.actionOnItemAtPosition<TeamAdapter.TeamViewHolder>(2, click())
        )

        onView(withId(R.id.view_pager_id)).check(matches(isDisplayed()))
        onView(allOf(withText("News"), isDisplayed())).check(matches(isDisplayed()))


        onView(withId(R.id.appbar)).perform(swipeUp())
        onView(withId(R.id.view_pager_id)).perform(swipeRight()).check(matches(isDisplayed()))

        onView(withId(R.id.team_name_txt_id)).check(matches(withText("Zontrax")))

        onView(allOf(withText("Players"), isDisplayed())).check(matches(isDisplayed()))
        onView(
            allOf(withId(R.id.recycler_view_id), isDisplayed())
        ).perform(
            RecyclerViewActions.actionOnItemAtPosition<TeamAdapter.TeamViewHolder>(0, click())
        )

        onView(
            allOf(
                withId(R.id.players_name),
                withText("Lorene Rego")
            )
        ).check(matches(withText("Lorene Rego")))
    }
}