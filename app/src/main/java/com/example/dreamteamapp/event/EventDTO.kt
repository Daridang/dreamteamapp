package com.example.dreamteamapp.event

import com.google.gson.annotations.SerializedName

data class Events(@SerializedName("results") val events: List<EventAPI>)

data class EventAPI(
    val idEvent: Int = 0,
    val idSoccerXML: String = "",
    val idAPIfootball: String = "",
    val strEvent: String = "",
    val strEventAlternate: String = "",
    val strFilename: String = "",
    val strSport: String = "",
    val idLeague: Int = 0,
    val strLeague: String = "",
    val strSeason: Int = 0,
    val strDescriptionEN: String = "",
    val strHomeTeam: String = "",
    val strAwayTeam: String = "",
    val intHomeScore: String = "",
    val intRound: Int = 0,
    val intAwayScore: String = "",
    val intSpectators: String = "",
    val strHomeGoalDetails: String = "",
    val strHomeRedCards: String = "",
    val strHomeYellowCards: String = "",
    val strHomeLineupGoalkeeper: String = "",
    val strHomeLineupDefense: String = "",
    val strHomeLineupMidfield: String = "",
    val strHomeLineupForward: String = "",
    val strHomeLineupSubstitutes: String = "",
    val strHomeFormation: String = "",
    val strAwayRedCards: String = "",
    val strAwayYellowCards: String = "",
    val strAwayGoalDetails: String = "",
    val strAwayLineupGoalkeeper: String = "",
    val strAwayLineupDefense: String = "",
    val strAwayLineupMidfield: String = "",
    val strAwayLineupForward: String = "",
    val strAwayLineupSubstitutes: String = "",
    val strAwayFormation: String = "",
    val intHomeShots: String = "",
    val intAwayShots: String = "",
    val dateEvent: String = "",
    val dateEventLocal: String = "",
    val strDate: String = "",
    val strTime: String = "",
    val strTimeLocal: String = "",
    val strTVStation: String = "",
    val idHomeTeam: Int = 0,
    val idAwayTeam: Int = 0,
    val strResult: String = "",
    val strCircuit: String = "",
    val strCountry: String = "",
    val strCity: String = "",
    val strPoster: String = "",
    val strFanart: String = "",
    val strThumb: String = "",
    val strBanner: String = "",
    val strMap: String = "",
    val strTweet1: String = "",
    val strTweet2: String = "",
    val strTweet3: String = "",
    val strVideo: String = "",
    val strLocked: String = ""
)

fun Events.asDomainModel(): List<Event> {
    return events.map {
        Event(
            idEvent = it.idEvent,
            idLeague = it.idLeague,
            strHomeTeam = it.strHomeTeam,
            strAwayTeam = it.strAwayTeam,
            idHomeTeam = it.idHomeTeam,
            idAwayTeam = it.idAwayTeam,
            dateEvent = it.dateEvent
        )
    }
}

fun Events.asDataBaseModel(): List<EventDb> {
    return events.map {
        EventDb(
            idEvent = it.idEvent,
            idLeague = it.idLeague,
            strHomeTeam = it.strHomeTeam,
            strAwayTeam = it.strAwayTeam,
            idHomeTeam = it.idHomeTeam,
            idAwayTeam = it.idAwayTeam,
            dateEvent = it.dateEvent
        )
    }
}