package com.example.dreamteamapp.utils


const val LEAGUE_ID = 4387

const val UPDATE_TIME_PREFS = "update_time_prefs"
const val TEAM_UPDATE = "team_update_time"
const val EVENT_UPDATE = "event_update_time"
const val PLAYER_UPDATE = "player_update_time"

const val BASE_URL = "https://www.thesportsdb.com/api/v1/json/1/"