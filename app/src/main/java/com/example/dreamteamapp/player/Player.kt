package com.example.dreamteamapp.player


data class Player(

    var idPlayer: Int? = 0,
    var idTeam: Int? = 0,
    var strPlayer: String? = "",
    var dateBorn: String? = "",
    var strDescriptionEN: String? = "",
    var strPosition: String? = "",
    var strHeight: String? = "",
    var strWeight: String? = "",
    var strThumb: String? = "",
    var strCutout: String? = ""
)

fun List<Player>.asDatabaseModel(): List<PlayerDb> {
    return map {
        PlayerDb(
            idPlayer = it.idPlayer,
            idTeam = it.idTeam,
            strPlayer = it.strPlayer,
            dateBorn = it.dateBorn,
            strDescriptionEN = it.strDescriptionEN,
            strPosition = it.strPosition,
            strHeight = it.strHeight,
            strWeight = it.strWeight,
            strThumb = it.strThumb,
            strCutout = it.strCutout
        )
    }
}