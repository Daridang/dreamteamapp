package com.example.dreamteamapp.team

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.dreamteamapp.model.repository.TeamRepository
import javax.inject.Inject


class TeamViewModel @Inject constructor(private val repo: TeamRepository) : ViewModel() {

    fun getTeamList(): LiveData<List<Team>> = repo.getList()
}