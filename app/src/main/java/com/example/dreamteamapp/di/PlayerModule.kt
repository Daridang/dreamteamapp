package com.example.dreamteamapp.di

import androidx.lifecycle.ViewModel
import com.example.dreamteamapp.player.PlayerDetailViewModel
import com.example.dreamteamapp.player.PlayerViewModel
import com.example.dreamteamapp.player.PlayersDetailsFragment
import com.example.dreamteamapp.player.PlayersFragment
import com.example.dreamteamapp.utils.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class PlayerModule {

    @ContributesAndroidInjector
    abstract fun contributePlayersFragment(): PlayersFragment

    @ContributesAndroidInjector
    abstract fun contributePlayersDetailsFragment(): PlayersDetailsFragment

    @IntoMap
    @ViewModelKey(PlayerViewModel::class)
    @Binds
    abstract fun bindPlayerViewModel(vm: PlayerViewModel): ViewModel

    @IntoMap
    @ViewModelKey(PlayerDetailViewModel::class)
    @Binds
    abstract fun bindPlayerDetailViewModel(vm: PlayerDetailViewModel): ViewModel
}