package com.example.dreamteamapp.di

import androidx.lifecycle.ViewModel
import com.example.dreamteamapp.event.EventViewModel
import com.example.dreamteamapp.event.EventsFragment
import com.example.dreamteamapp.utils.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class EventModule {

    @ContributesAndroidInjector
    abstract fun contributeEventsFragment(): EventsFragment

    @IntoMap
    @ViewModelKey(EventViewModel::class)
    @Binds
    abstract fun bindEventViewModel(vm: EventViewModel): ViewModel

}